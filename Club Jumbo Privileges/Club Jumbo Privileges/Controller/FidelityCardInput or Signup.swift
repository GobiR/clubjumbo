//
//  FidelityCardInput or Signup.swift
//  Club Jumbo Privileges
//
//  Created by Vengatesh C. on 19/04/18.
//

import UIKit

class FidelityCardInput_or_Signup: MenuDisplayControllerViewController,UINavigationControllerDelegate {

    @IBOutlet weak var toBarcodeInputOutlet: UIButton!
    @IBOutlet weak var toFidelitySignupOutlet: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        toBarcodeInputOutlet.imageView?.contentMode = .scaleToFill
        toFidelitySignupOutlet.imageView?.contentMode = .scaleToFill
        toBarcodeInputOutlet.layer.cornerRadius = 10
        toFidelitySignupOutlet.layer.cornerRadius = 10
        customNavigationBar()
        backButton()
    }

    override func viewWillAppear(_ animated: Bool) {
        customNavigationBar()
        backButton()
    }
    
    func customNavigationBar()
    {
        self.navigationController?.navigationBar.topItem?.title = "";
        self.navigationItem.title = "Ma Carte Privilèges"
    }
    
    @objc func sidemenu(sender:UIButton)
    {
        self.openVCSlideMenuButtonPressed(sender as UIButton)
        print("Slidemenu Clicked")
    }

    @IBAction func toBarcodeInputView(_ sender: UIButton) {
        let barcodeInputView = storyboard?.instantiateViewController(withIdentifier: "BarcodeInput") as! BarcodeInput
        navigationController?.pushViewController(barcodeInputView, animated: true)
    }
    
    @IBAction func toFidelityCardsignpView(_ sender: UIButton) {
        let webView = storyboard?.instantiateViewController(withIdentifier: "WebViewJnViewController") as! WebViewJnViewController
        webView.url = constant.fidalityCardSignupURL
        webView.pageTitle = constant.clubJumbo
        navigationController?.pushViewController(webView, animated: true)
    }
    
}
