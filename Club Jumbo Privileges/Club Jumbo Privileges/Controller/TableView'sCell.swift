//
//  TableView'sCell.swift
//  Club Jumbo Privileges
//
//  Created by Gobi R. on 19/04/18.
//

import UIKit
import CoreData

class TableView_sCell: UITableViewCell {

    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    let request: NSFetchRequest<BarcodeCoreData> = BarcodeCoreData.fetchRequest()
    var coreData = [BarcodeCoreData]()
    
    var delteImage = UIImageView()
    var numberLabel = UILabel()
    var userNameLabel = UILabel()
    var id = Int()
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func layoutSubviews() {
          self.imageView?.frame = CGRect(x: 0, y: 0, width: Int(self.contentView.layer.bounds.width), height: Int(self.contentView.layer.bounds.height-5 ))
            self.imageView?.contentMode = .scaleToFill
    }
    
//    override func layoutSubviews() {
//        do {
//            coreData = try context.fetch(request)
//            if coreData.count > 0{
//                if id == 0 {
//        self.imageView?.frame = CGRect(x: 0, y: 0, width: Int(self.contentView.layer.bounds.width), height: Int(self.contentView.layer.bounds.height-5 ))
//        self.imageView?.contentMode = .scaleAspectFit
//                }
//            }
//        } catch {
//            print(error)
//        }
//        
//            self.imageView?.frame = CGRect(x: 0, y: 0, width: Int(self.contentView.layer.bounds.width), height: Int(self.contentView.layer.bounds.height-5 ))
//            self.imageView?.contentMode = .scaleAspectFill
//        
//    }
    
    // MARK:- Creating Image and labels 
    func createDeleteImage() {
        
        self.delteImage = UIImageView(frame: CGRect(x: self.contentView.bounds.width - 50, y: self.contentView.bounds.height - 30, width: 20, height: 20))
        delteImage.image = UIImage(named: "delete_grey")
      
        self.imageView?.image = constant.barcodeImageFile
        
        self.numberLabel = UILabel(frame: CGRect(x: 0, y: self.contentView.bounds.height-35, width:contentView.bounds.width, height: 35 ))
      //  numberLabel.backgroundColor = UIColor.green
      //  numberLabel.center = CGPoint(x:(self.contentView.bounds.width/2) - 30, y: self.contentView.bounds.height-25)
        
        numberLabel.adjustsFontForContentSizeCategory = true
        numberLabel.adjustsFontSizeToFitWidth = true
        numberLabel.font = UIFont.boldSystemFont(ofSize: 22)
        numberLabel.textAlignment = .center
        self.contentView.addSubview(numberLabel)
        numberLabel.text = UserDefaults.standard.value(forKey: "barcodeString") as? String
        
        self.userNameLabel = UILabel(frame: CGRect(x: 0, y: 2, width: self.contentView.bounds.width , height: 30))
     //   userNameLabel.center = CGPoint(x:self.contentView.bounds.width-240, y: self.contentView.bounds.height-250)
       self.userNameLabel.textAlignment = .center
         userNameLabel.font = UIFont.boldSystemFont(ofSize:18)
        self.contentView.addSubview(userNameLabel)
       
        userNameLabel.text = UserDefaults.standard.value(forKey: "userName") as? String
        self.imageView?.addSubview(delteImage)
         userNameLabel.bringSubview(toFront: self.imageView!)
        delteImage.isHidden = false
        numberLabel.isHidden = false
        userNameLabel.isHidden = false
    }

}

