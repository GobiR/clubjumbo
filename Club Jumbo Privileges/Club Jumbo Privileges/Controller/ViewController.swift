//
//  ViewController.swift
//  Club Jumbo Privileges
//
//  Created by Gobi R. on 18/04/18.
//

import UIKit
import CoreData

class ViewController: MenuDisplayControllerViewController, UITableViewDataSource,UITableViewDelegate {
   
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var magasinsButton: UIButton!
    @IBOutlet weak var contacterButton: UIButton!
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    let request: NSFetchRequest<BarcodeCoreData> = BarcodeCoreData.fetchRequest()
    
    var imageArray = [UIImage]()
    var coreData = [BarcodeCoreData]()
    let model = DataModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        customNavigationBar()
        imageArray = [UIImage(named: "ma_carte_privelege_bg")!,UIImage(named: "shopping_cart_bg")!]
        print(imageArray.count)
        tableView.backgroundColor = UIColor.yellow
        magasinsButton.imageView?.contentMode = .scaleToFill
        contacterButton.imageView?.contentMode = .scaleToFill
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        jumboSparLogo()
//        model.loadContext()
        model.retriveData()
        tableView.reloadData()
        navigationController?.navigationBar.barTintColor = UIColor(red: 254/255, green: 81/255, blue: 82/255, alpha: 1.0)
    }
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return imageArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! TableView_sCell
      
        cell.imageView?.image = imageArray[indexPath.row]
        cell.imageView?.isUserInteractionEnabled = true
         cell.imageView?.contentMode = .scaleToFill
        cell.imageView?.clipsToBounds = true
        cell.delteImage.isHidden = true
        cell.numberLabel.isHidden = true
        cell.userNameLabel.isHidden = true
        
//        do {
//        coreData = try context.fetch(request)
//        if coreData.count > 0{
        // {
        let d = model.checkEmptyDatabase()
        if d != 0 {
            if indexPath.row == 0 {
                cell.imageView?.contentMode = .scaleAspectFit
                cell.createDeleteImage()
                cell.id = indexPath.row
      // }
            }}
//            }}
//            catch {
//            print("Error:\(error)")
//        }
        return cell
        }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return tableView.layer.bounds.height/2
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
         let d = model.checkEmptyDatabase()
        let webView = storyboard?.instantiateViewController(withIdentifier: "WebViewJnViewController") as! WebViewJnViewController
        if indexPath.row == 1 {
            webView.url = constant.promotionsURL
            webView.pageTitle = constant.clubJumbo
            navigationController?.pushViewController(webView, animated: true)
            
        }
//        else if coreData.count > 0
        else if d != 0 {
            if indexPath.row == 0 {
                
                let removeBarcodeView = storyboard?.instantiateViewController(withIdentifier: "RemoveBarcodeViewController") as! RemoveBarcodeViewController
                navigationController?.pushViewController(removeBarcodeView, animated: true)
                
            }
        } else {
            let fidelitySignupView = storyboard?.instantiateViewController(withIdentifier: "FidelityCardInput_or_Signup") as! FidelityCardInput_or_Signup
            navigationController?.pushViewController(fidelitySignupView, animated: true)
        }
    }
    
    @IBAction func contactinfoButton(_ sender: UIButton) {
        toWebView(typeURL: constant.contatctFormURl, title: constant.clubJumbo)
        
    }
    
    @IBAction func magasinsButton(_ sender: UIButton) {

        toWebView(typeURL: constant.magasinsURL, title: constant.clubJumbo)
    }
    
    func toWebView(typeURL: String, title: String) {
        
        let webView = storyboard?.instantiateViewController(withIdentifier: "WebViewJnViewController") as! WebViewJnViewController
        if typeURL.isEmpty {
            print("URL is Empty")
        }else {
            webView.url = typeURL
            webView.pageTitle = title
            navigationController?.pushViewController(webView, animated: true)
        }
    
    }
    
    func customNavigationBar()
    {
        let backButton = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        backButton.setBackgroundImage(UIImage(named: "Menu"), for: .normal)
        backButton.addTarget(self, action: #selector(sidemenu(sender:)), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @objc func sidemenu(sender:UIButton)
    {
        self.openVCSlideMenuButtonPressed(sender as UIButton)
        print("Slidemenu Clicked")
    }
    
    //MARK: Logos
    func jumboSparLogo() {
        let jumboLogo = UIImageView(image: UIImage(named: "jumbo"))
        let logo1 = UIBarButtonItem(customView: jumboLogo)
        let sparLogo = UIImageView(image: UIImage(named: "spar"))
        let logo2 = UIBarButtonItem(customView: sparLogo)
        self.navigationItem.setLeftBarButtonItems([logo1,logo2], animated: true)
        jumboLogo.isUserInteractionEnabled = false
        sparLogo.isUserInteractionEnabled = false
        
    }
    
}

   
