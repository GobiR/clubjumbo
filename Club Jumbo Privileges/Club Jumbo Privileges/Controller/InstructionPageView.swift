//
//  InstructionPageView.swift
//  Club Jumbo Privileges
//
//  Created by Vengatesh C. on 20/04/18.
//


import UIKit

class InstructionPageView: UIPageViewController, UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    
    
    var pageControl = UIPageControl()
    
    
    // MARK: UIPageViewControllerDataSource
    
    lazy var orderedViewControllers: [UIViewController] = {
        return [self.newVc(viewController: "Instruction1"),
                self.newVc(viewController: "Instruction2"),self.newVc(viewController: "Instruction3"),self.newVc(viewController: "Instruction4"),self.newVc(viewController: "Instruction5")]
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataSource = self
        self.delegate = self
      
        let profileButton = UIButton()
        profileButton.frame = CGRect(x:0, y:0, width:60, height:20)
        profileButton.setTitle("Skip", for: .normal)
        profileButton.setTitle("Skip", for: .highlighted)
        
        profileButton.backgroundColor =  UIColor(red: 255/255, green: 81/255, blue: 50/255, alpha: 1.0)
        profileButton.layer.cornerRadius = 5.0
        profileButton.titleLabel?.font = UIFont(name: "", size: 10.0)
        
        profileButton.addTarget(self, action: #selector(skip), for: .touchUpInside)
        
        let rightBarButton = UIBarButtonItem(customView: profileButton)
        self.navigationItem.rightBarButtonItem = rightBarButton
        
        
        // This sets up the first view that will show up on our page control
        if let firstViewController = orderedViewControllers.first {
            setViewControllers([firstViewController],
                               direction: .forward,
                               animated: true,
                               completion: nil)
        }
        configurePageControl()
    }
    
    override func viewWillAppear(_ animated: Bool) {
       
        navigationController?.navigationBar.barTintColor = UIColor.black
        self.navigationItem.setHidesBackButton(true, animated: true)
        
    }
    
//    override func viewWillDisappear(_ animated: Bool) {
//        navigationController?.navigationBar.barTintColor = UIColor(red: 255/255, green: 81/255, blue: 50/255, alpha: 1.0)
//    }
    
  @objc func skip()
    {
       navigationController?.popToRootViewController(animated: true)
    }
    
    
    func configurePageControl() {
        
        // The total number of pages that are available is based on how many available colors we have.
        
        let bounds = self.navigationController!.navigationBar.bounds
        print(bounds)
        print(UIScreen.main.bounds.maxY)
        print(self.navigationController!.navigationBar.bounds.height)
        pageControl = UIPageControl(frame: CGRect(x: 0,y: UIScreen.main.bounds.maxY - 150,width: UIScreen.main.bounds.width,height: 100))
        self.pageControl.numberOfPages = orderedViewControllers.count
        self.pageControl.currentPage = 0
        self.pageControl.tintColor = UIColor.black
        self.pageControl.pageIndicatorTintColor = UIColor.white
        self.pageControl.currentPageIndicatorTintColor = UIColor.black
        self.view.addSubview(pageControl)
    }
    
    func newVc(viewController: String) -> UIViewController {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: viewController)
    }
    
    
    // MARK: Delegate methords
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
       
        let pageContentViewController = pageViewController.viewControllers![0]
        self.pageControl.currentPage = orderedViewControllers.index(of: pageContentViewController)!
    }
    
    // MARK: Data source functions.
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
            return nil
        }
        
        let previousIndex = viewControllerIndex - 1
        
        guard previousIndex >= 0 else {
           
            // return orderedViewControllers.last
            
            return nil
        }
        
        guard orderedViewControllers.count > previousIndex else {
            return nil
        }
        
        return orderedViewControllers[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
            return nil
        }
        
        let nextIndex = viewControllerIndex + 1
        let orderedViewControllersCount = orderedViewControllers.count
        
       
        guard orderedViewControllersCount != nextIndex else {
//            return orderedViewControllers.first
           
             return nil
        }
        
        guard orderedViewControllersCount > nextIndex else {
            return nil
        }
        
        return orderedViewControllers[nextIndex]
    }
    
    
}

